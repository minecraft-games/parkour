package com.whatthefox.minecraft.parkour;

import com.whatthefox.minecraft.gamecore.entity.GameManagerInfo;
import com.whatthefox.minecraft.gamecore.entity.GameState;
import com.whatthefox.minecraft.gamecore.entity.Observer;
import com.whatthefox.minecraft.gamecore.entity.RepeatingTask;
import java.util.List;

import com.whatthefox.minecraft.gamecore.manager.GameCoreManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public class ParkourPlugin extends JavaPlugin implements Observer {

  public static ParkourPlugin getInstance() {
    return getPlugin(ParkourPlugin.class);
  }

  FileConfiguration config = getConfig();

  @Override
  public void onEnable() {
    if (GameCoreManager.getState() == GameState.INIT) {
      getLogger().info("Calling GameCoreManager init() and subscribe()");

      GameCoreManager.init(new GameManagerInfo(1, 5, Bukkit.getWorlds().get(0).getSpawnLocation(), "CoreTest"));
      GameCoreManager.subscribe(ParkourPlugin.getInstance());
    }

    getLogger().info("Hello " + config.getString("test"));
    // Bukkit.getWorlds().get(0).getBlockAt(0,0,0).setType(Material.AIR);
/*    else if (GameCoreManager.getState() == GameState.IN_PROGRESS) {
      getLogger().info("Calling GameCoreManager endGame()");
      GameCoreManager.endGame();
    } else {
      getLogger().info("GameCoreManager can't be called in this state");
    }*/
  }

  @Override
  public void startGame(final List<Player> list, final List<Player> spectators) {
    Bukkit.broadcastMessage("Début de la partie !");
    Bukkit.broadcastMessage("");
    getLogger().info("Receiving event startGame()");
    getLogger().info("Players: " + list.stream().map(Player::getName).toList());
    startCountdown();
  }

  private void startCountdown() {
    Bukkit.broadcastMessage("Voici un exemple de timer");
    getLogger().info("Starting countdown");
    new RepeatingTask(this, 0, 20) {
      private final int max = 3;
      private int count = max;

      @Override
      public void run() {
        if (count == 0) {
          Bukkit.broadcastMessage("Fin de l'exemple !");
          Bukkit.broadcastMessage("");
          Bukkit.broadcastMessage("Pour terminer la partie, utilisez la commande /tester");
          cancelLoop();
        } else {
          Bukkit.broadcastMessage("Fin de l'exemple dans " + count + " secondes");
          count--;
        }
      }
    };
  }
}
